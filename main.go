package main

import (
	"io/ioutil"
	"runtime"
	"unsafe"

	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/mathgl/mgl32"
)

func init() {
	runtime.LockOSThread()
}

func framebufferSizeCallback(win *glfw.Window, w, h int) {
	gl.Viewport(0, 0, int32(w), int32(h))
}

func processInputs(win *glfw.Window) {
	if win.GetKey(glfw.KeyEscape) == glfw.Press {
		win.SetShouldClose(true)
	}
}

func main() {
	err := glfw.Init()
	if err != nil {
		panic(err)
	}
	defer glfw.Terminate()

	glfw.WindowHint(glfw.ContextVersionMajor, 3)
	glfw.WindowHint(glfw.ContextVersionMinor, 3)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)

	win, err := glfw.CreateWindow(800, 600, "Learn OpenGL", nil, nil)
	if err != nil {
		panic(err)
	}
	win.MakeContextCurrent()
	win.SetFramebufferSizeCallback(framebufferSizeCallback)

	if err := gl.Init(); err != nil {
		panic(err)
	}

	// Define quad.
	vertices := []float32{
		// positions    // texture coords
		0.5, 0.5, 0.0, 1.0, 1.0, // top right
		0.5, -0.5, 0.0, 1.0, 0.0, // bottom right
		-0.5, -0.5, 0.0, 0.0, 0.0, // bottom left
		-0.5, 0.5, 0.0, 0.0, 1.0, // top left
	}

	indices := []uint32{
		0, 1, 3,
		1, 2, 3,
	}

	var vbo uint32
	gl.GenBuffers(1, &vbo)

	var ebo uint32
	gl.GenBuffers(1, &ebo)

	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, int(unsafe.Sizeof(float32(0)))*len(vertices), gl.Ptr(vertices), gl.STATIC_DRAW)

	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, ebo)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, int(unsafe.Sizeof(float32(0)))*len(indices), gl.Ptr(indices), gl.STATIC_DRAW)

	// Tell OpenGL how vertex data looks like.
	// position
	gl.VertexAttribPointer(0, 3, gl.FLOAT, false, int32(unsafe.Sizeof(float32(0)))*5, gl.PtrOffset(0))
	gl.EnableVertexAttribArray(0)
	// texture
	gl.VertexAttribPointer(1, 2, gl.FLOAT, false, int32(unsafe.Sizeof(float32(0)))*5, gl.PtrOffset(3*int(unsafe.Sizeof(float32(0)))))
	gl.EnableVertexAttribArray(1)

	// Load textures from file.
	texture1, err := NewTexture("container.jpg")
	if err != nil {
		panic(err)
	}
	texture2, err := NewTexture("awesomeface.png")
	if err != nil {
		panic(err)
	}

	// Load shaders from file.
	vShaderStr, err := ioutil.ReadFile("shader.vs")
	if err != nil {
		panic(err)
	}

	fShaderStr, err := ioutil.ReadFile("shader.fs")
	if err != nil {
		panic(err)
	}

	shader, err := NewShader(string(vShaderStr), string(fShaderStr))
	if err != nil {
		panic(err)
	}

	// Use the right textures.
	shader.Use()
	shader.SetInt("texture1\x00", 0)
	shader.SetInt("texture2\x00", 1)

	for !win.ShouldClose() {
		processInputs(win)

		gl.ClearColor(0, 0, 0, 1.0)
		gl.Clear(gl.COLOR_BUFFER_BIT)

		gl.ActiveTexture(gl.TEXTURE0)
		gl.BindTexture(gl.TEXTURE_2D, texture1.ID)
		gl.ActiveTexture(gl.TEXTURE1)
		gl.BindTexture(gl.TEXTURE_2D, texture2.ID)

		// Transformations.
		trans := mgl32.Translate3D(0.5, -0.5, 0)
		rot := mgl32.HomogRotate3D(float32(glfw.GetTime()), mgl32.Vec3{0, 0, 1})
		all := trans.Mul4(rot)

		shader.Use()
		transLoc := gl.GetUniformLocation(shader.ID, gl.Str("transform\x00"))
		gl.UniformMatrix4fv(transLoc, 1, false, &all[0])

		gl.BindVertexArray(vao)
		// gl.DrawArrays(gl.TRIANGLES, 0, 3)
		gl.DrawElements(gl.TRIANGLES, 6, gl.UNSIGNED_INT, nil)
		// gl.BindVertexArray(0)

		win.SwapBuffers()
		glfw.PollEvents()
	}

	// Deallocate
	gl.DeleteVertexArrays(1, &vao)
	gl.DeleteBuffers(1, &ebo)
	gl.DeleteBuffers(1, &vbo)
	glfw.Terminate()
}
