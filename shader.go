package main

import (
	"fmt"

	"github.com/go-gl/gl/v3.3-core/gl"
)

// Shader represents an OpenGL shader program.
type Shader struct {
	ID uint32
}

// NewShader creates and returns a new shader program.
func NewShader(vShaderStr, fShaderStr string) (*Shader, error) {
	s := &Shader{}

	// Compile shaders.
	var success int32
	// 1. vertex shader
	vertex := gl.CreateShader(gl.VERTEX_SHADER)
	src, free := gl.Strs(vShaderStr)
	length := int32(len(vShaderStr))
	gl.ShaderSource(vertex, 1, src, &length)
	free()
	gl.CompileShader(vertex)
	// Test for compile errors.
	gl.GetShaderiv(vertex, gl.COMPILE_STATUS, &success)
	if success == gl.FALSE {
		log := [512]uint8{}
		gl.GetShaderInfoLog(vertex, 512, nil, &log[0])
		return nil, fmt.Errorf("problem compiling vertex shader: %s", gl.GoStr(&log[0]))
	}
	// 2. fragment shader
	fragment := gl.CreateShader(gl.FRAGMENT_SHADER)
	src, free = gl.Strs(fShaderStr)
	length = int32(len(fShaderStr))
	gl.ShaderSource(fragment, 1, src, &length)
	free()
	gl.CompileShader(fragment)
	// Test compile errors.
	gl.GetShaderiv(fragment, gl.COMPILE_STATUS, &success)
	if success == gl.FALSE {
		log := [512]uint8{}
		gl.GetShaderInfoLog(fragment, 512, nil, &log[0])
		return nil, fmt.Errorf("problem compiling fragment shader: %s", gl.GoStr(&log[0]))
	}

	// Shader program.
	s.ID = gl.CreateProgram()
	gl.AttachShader(s.ID, vertex)
	gl.AttachShader(s.ID, fragment)
	gl.LinkProgram(s.ID)

	// Test for linking errors.
	gl.GetProgramiv(s.ID, gl.LINK_STATUS, &success)
	if success == gl.FALSE {
		log := [512]uint8{}
		gl.GetProgramInfoLog(s.ID, 512, nil, &log[0])
		return nil, fmt.Errorf("problem linking program; %s", gl.GoStr(&log[0]))
	}
	gl.DeleteShader(vertex)
	gl.DeleteShader(fragment)

	return s, nil
}

// Use activates a shader.
func (s *Shader) Use() {
	gl.UseProgram(s.ID)
}

// SetBool sets a bool uniform by name.
func (s *Shader) SetBool(name string, val bool) {
	v := int32(0)
	if val {
		v = 1
	}
	gl.Uniform1i(gl.GetUniformLocation(s.ID, gl.Str(name)), v)
}

// SetInt sets a int uniform by name.
func (s *Shader) SetInt(name string, val int32) {
	gl.Uniform1i(gl.GetUniformLocation(s.ID, gl.Str(name)), val)
}

// SetFloat sets a float uniform by name.
func (s *Shader) SetFloat(name string, val float32) {
	gl.Uniform1f(gl.GetUniformLocation(s.ID, gl.Str(name)), val)
}
